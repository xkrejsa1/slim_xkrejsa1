<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/',
    function (Request $request, Response $response, $args) {
        $q = $request->getQueryParam('q');      //neco jako $_GET
        try{
            if (empty($q)) {
                $stmt = $this->db->prepare('SELECT person.*, 
                                                    COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa,
                                                    a.pocet_kontaktu as pocet_kontaktu,  COALESCE(b.pocet_vztahu,0) + COALESCE(c.pocet_vztahu,0) as pocet_vztahu, d.pocet_schuzek as pocet_schuzek
                                            FROM person
                                            LEFT JOIN location USING(id_location)
                                            LEFT JOIN 
                                            (
                                                 SELECT  id_person, COUNT(id_contact) as pocet_kontaktu
                                                 FROM contact
                                                 GROUP BY id_person
                                            ) AS a  
                                            USING (id_person)
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person1, COUNT(id_relation) as pocet_vztahu
                                                 FROM relation
                                                 GROUP BY id_person1
                                            ) AS b 
                                            ON person.id_person = b.id_person1 
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person2, COUNT(id_relation) as pocet_vztahu
                                                 FROM relation
                                                 GROUP BY id_person2
                                            ) AS c 
                                            ON person.id_person = c.id_person2
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person, COUNT(id_meeting) as pocet_schuzek
                                                 FROM person_meeting
                                                 GROUP BY id_person
                                            ) AS d 
                                            ON person.id_person = d.id_person
                                            ORDER BY last_name ASC');
            } else {
                $stmt = $this->db->prepare('SELECT person.*, 
                                                    COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa,
                                                    a.pocet_kontaktu as pocet_kontaktu,  COALESCE(b.pocet_vztahu,0) + COALESCE(c.pocet_vztahu,0) as pocet_vztahu, d.pocet_schuzek as pocet_schuzek
                                            FROM person
                                            LEFT JOIN location USING(id_location)
                                            LEFT JOIN 
                                            (
                                                 SELECT  id_person, COUNT(id_contact) as pocet_kontaktu
                                                 FROM contact
                                                 GROUP BY id_person
                                            ) AS a  
                                            USING (id_person)
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person1, COUNT(id_relation) as pocet_vztahu
                                                 FROM relation
                                                 GROUP BY id_person1
                                            ) AS b 
                                            ON person.id_person = b.id_person1 
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person2, COUNT(id_relation) as pocet_vztahu
                                                 FROM relation
                                                 GROUP BY id_person2
                                            ) AS c 
                                            ON person.id_person = c.id_person2 
                                            LEFT JOIN 
                                            (
                                                 SELECT id_person, COUNT(id_meeting) as pocet_schuzek
                                                 FROM person_meeting
                                                 GROUP BY id_person
                                            ) AS d 
                                            ON person.id_person = d.id_person
                                            WHERE last_name ILIKE :q 
                                            OR first_name ILIKE :q 
                                            ORDER BY last_name ASC');
                $stmt->bindValue(':q', $q . '%');
            }
            $stmt->execute();
        }
        catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }

        $tplVars['osoby'] = $stmt->fetchAll();
        $tplVars['q'] = $q;

        return $this->view->render(
            $response,
            "osoby.latte",
            $tplVars);
    })->setName("osoby");



$app->post('/smaz-osobu', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try{
        $stmt = $this->db->prepare('DELETE FROM person WHERE id_person=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('osoby'));
})->setName('smazOsobu');


/*editace*/
$app->get('/uprav-osobu', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try{
        $stmt = $this->db->prepare('SELECT * FROM person WHERE id_person=:id');
        $stmt->bindValue(':id',$id);
        $stmt->execute();
    }
    catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare('SELECT id_location, 
                                    COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa 
                                    FROM location ORDER BY 2 ASC');
        $stmt2 ->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    $person = $stmt->fetch();
    $tplVars['form']= [
                'ln' => $person['last_name'],
                'nn' => $person['nickname'],
                'fn' => $person['first_name'],
                'g' => $person['gender'],
                'bd' => $person['birth_day'],
                'h' => $person['height'],
                'idl' => $person['id_location']
    ];
    $tplVars['locations']=$stmt2->fetchAll();

    $tplVars['id']=$id;
    return $this->view->render($response,'uprav-osobu.latte',$tplVars);
})->setName('upravOsobu');

/*ulozeni zmen*/
$app->post('/uprav-osobu', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    try{
        $stmt = $this->db->prepare('UPDATE person 
                                    SET   first_name = :fn,
                                          last_name = :ln,
                                          nickname = :nn,
                                          gender= :g,
                                          birth_day = :bd,
                                          height = :h,
                                          id_location = :idl
                                    WHERE id_person=:id');
        $stmt->bindValue(':fn', $data['fn']);
        $stmt->bindValue(':ln', $data['ln']);
        $stmt->bindValue(':nn', $data['nn']);
        $stmt->bindValue(':g', empty($data['g']) ? null :$data['g']);
        $stmt->bindValue(':bd', empty($data['bd']) ? null :$data['bd']);
        $stmt->bindValue(':h', empty($data['h']) ? null :$data['h']);
        $stmt->bindValue(':idl', empty($data['idl']) ? null :$data['idl']);


        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex)
    {if($ex->getCode() == 23505) {
        $tplVars['error']='Tato osoba už existuje';
        $tplVars['form']=$data;
        return $this->view->render(
            $response,
            "uprav-osobu.latte",
            $tplVars);
    }else {
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }}

    return $response->withHeader('Location',$this->router->pathFor('osoby'));
});


$app->get('/pridej-osobu-s-adresou', function (Request $request, Response $response, $args) {
    $tplVars['form']= [ 'ln' => '',
                        'nn' => '',
                        'fn' => '',
                        'ci' => '',
                        'st' => '',
                        'sn' => '',
                        'zip' => ''
                        ];

    try{
        $stmt = $this->db->prepare('SELECT id_location, 
                                    COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa 
                                    FROM location ORDER BY 2 ASC');
        $stmt ->execute();

    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    $tplVars['locations']=$stmt->fetchAll();

    return $this->view->render(
        $response,
        "pridej-osobu-s-adresou.latte",
        $tplVars);

})->setName("pridejOsobuSAdresou");

$app->post('/pridej-osobu-s-adresou', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();      //neco jako $_POST
    /*
    print_r($data);
    exit;
    */
    try{
        $stmt = $this->db->prepare('SELECT COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa FROM location');
        //$stmt ->execute();
        $tplVars['locations']=$stmt->fetchAll();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    if (!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])){
        try{
            $this->db->beginTransaction();
            /*
            $g = $data['g'];
            if (empty($data['g'])){
                $g=null;
            }*/

            $idlocation = null;
            if(($data['idl'])=="pna") {
                if(!empty($data['ci'])) {
                    //přidávám s adresou rovnou i novou adresu
                    $ci = empty($data['ci']) ? null : $data['ci'];
                    $st = empty($data['st']) ? null : $data['st'];
                    $sn = empty($data['sn']) ? null : $data['sn'];
                    $zip = empty($data['zip']) ? null : $data['zip'];

                    $stmt = $this->db->prepare('INSERT INTO location(city,street_name,street_number,zip)
                                                VALUES(:ci,:st,:sn,:zip)');
                    $stmt->bindValue(':ci', $ci);
                    $stmt->bindValue(':st', $st);
                    $stmt->bindValue(':sn', $sn);
                    $stmt->bindValue(':zip', $zip);
                    $stmt->execute();
                    $idlocation = $this->db->lastInsertId('location_id_location_seq');
                }
            }
            elseif ((!empty($data['idl'])) and ($data['idl']!='pba')){
                //přidávám už existující adresu
                $idlocation = $data['idl'];
            }
            //v jiných případech mi tam zůstane null a přidávám osobu bez adresy


            $g = empty($data['g']) ? null :$data['g'];
            $bd = empty($data['bd']) ? null :$data['bd'];
            $h = empty($data['h']) ? null :$data['h'];

            $stmt = $this->db->prepare('INSERT INTO person(first_name,last_name,nickname,gender,birth_day,height, id_location)
                                        VALUES(:fn,:ln,:nn,:g,:bd,:h,:idl)');
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->bindValue(':g', $g);
            $stmt->bindValue(':bd', $bd);
            $stmt->bindValue(':h', $h);
            $stmt->bindValue(':idl', $idlocation);
            $stmt->execute();
            $this->db->commit();
        }
        catch (Exception $ex) {
            $this->db->rollback();
            if($ex->getCode() == 23505) {
                $tplVars['error']='Tato osoba už existuje';
                $tplVars['form']=$data;
                return $this->view->render(
                    $response,
                    "pridej-osobu-s-adresou.latte",
                    $tplVars);
            }else {
                $this->logger->error($ex->getMessage());
                die ($ex->getMessage());
            }
        }
        return $response->withHeader(
            'Location',
            $this->router->pathFor('osoby'));
    }
    //data neprisly
    $tplVars['form']=$data;
    $tplVars['error']='Nejsou vyplněny všechny údaje';
    return $this->view->render(
        $response,
        "pridej-osobu-s-adresou.latte",
        $tplVars);

});
