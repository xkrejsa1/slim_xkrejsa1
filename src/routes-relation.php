<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/pridej-vztah', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if(empty($id)) {
        exit('Chybí id osoby');
    }
    try{
        $stmt = $this->db->prepare('SELECT * FROM relation_type');
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare(' SELECT id_relation, first_name || \' \' || last_name as other_person, description, relation_type.* 
                                      FROM relation 
                                      JOIN relation_type USING (id_relation_type) 
                                      JOIN person ON relation.id_person2 = person.id_person
                                      WHERE id_person1=:id
                                      UNION ALL
                                      SELECT id_relation, first_name || \' \' || last_name as other_person, description, relation_type.* 
                                      FROM relation 
                                      JOIN relation_type USING (id_relation_type) 
                                      JOIN person ON relation.id_person1 = person.id_person
                                      WHERE id_person2=:id
                                      ORDER BY name ASC, 2 ASC');
        $stmt2->bindValue(':id', $id);
        $stmt2->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt3 = $this->db->prepare('SELECT * FROM person WHERE id_person <> :id ORDER BY last_name');
        $stmt3->bindValue(':id', $id);
        $stmt3->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $tplVars['types'] = $stmt->fetchAll();
    $tplVars['relations'] = $stmt2->fetchAll();
    $tplVars['persons'] = $stmt3->fetchAll();
    $tplVars['id'] = $id;

    return $this->view->render(
        $response,
        "pridej-vztah.latte",
        $tplVars);

})->setName("pridejVztah");

$app->post('/pridej-vztah', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $des = empty($data['txt']) ? '' :$data['txt'];

    if(!empty($data['id']) && !empty($data['pr_id']) && !empty($data['rl_id'])) {
        try{
            $stmt = $this->db->prepare('INSERT INTO relation(id_person1,id_person2,id_relation_type,description)
                                        VALUES(:id_p,:id_pr,:id_rt,:des)');
            $stmt->bindValue(':id_p', $data['id']);
            $stmt->bindValue(':id_pr', $data['pr_id']);
            $stmt->bindValue(':id_rt', $data['rl_id']);
            $stmt->bindValue(':des', $des);
            $stmt->execute();
        }
        catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }
    }else{
        exit('Specify required parameters.');
    }
    return $response->withHeader(
        'Location',
        $this->router->pathFor('pridejVztah').'?id='.$data['id']);
})->setName("pridejVztah");


$app->post('/smaz-vztah', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $id_person = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('DELETE FROM relation WHERE id_relation=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('pridejVztah').'?id='.$id_person);
})->setName('smazVztah');



/*editace*/
$app->get('/uprav-vztah', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $idperson = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('SELECT id_relation, first_name || \' \' || last_name as other_person_name, person.id_person as other_person_id, description,relation_type.* 
                                    FROM relation 
                                    JOIN relation_type USING(id_relation_type) 
                                    JOIN person ON (person.id_person = relation.id_person1 AND relation.id_person2 =:idperson) OR (person.id_person = relation.id_person2 AND relation.id_person1 =:idperson)
                                    WHERE id_relation=:id');
        $stmt->bindValue(':id',$id);
        $stmt->bindValue(':idperson',$idperson);
        $stmt->execute();
    }
    catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare('SELECT * FROM relation_type');
        $stmt2->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt3 = $this->db->prepare('SELECT * FROM person WHERE id_person <> :id ORDER BY last_name');
        $stmt3->bindValue(':id', $idperson);
        $stmt3->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $relations = $stmt->fetch();
    $tplVars['form']= [
        'description' => $relations['description'],
        'type' => $relations['id_relation_type'],
        'other_person_name' => $relations['other_person_name']
    ];
    $tplVars['types'] = $stmt2->fetchAll();
    $tplVars['persons'] = $stmt3->fetchAll();
    $tplVars['id']=$id;
    $tplVars['idperson']=$idperson;
    return $this->view->render($response,'uprav-vztah.latte',$tplVars);
})->setName('upravVztah');

/*ulozeni zmen*/
$app->post('/uprav-vztah', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $idperson = $request->getQueryParam('idperson');
    $data = $request->getParsedBody();
    try{
        $stmt = $this->db->prepare('UPDATE relation 
                                    SET   description=:de,
                                          id_relation_type=:irt
                                    WHERE id_relation=:id');
        $stmt->bindValue(':de', $data['txt']);
        $stmt->bindValue(':irt', $data['irt']);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    return $response->withHeader('Location',$this->router->pathFor('pridejVztah').'?id='.$idperson);
    //return $response->withHeader('Location',$this->router->pathFor('osoby'));
});

