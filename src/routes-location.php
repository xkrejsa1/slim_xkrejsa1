<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/vytvorit-adresu', function (Request $request, Response $response, $args) {
    $tplVars['form']= [ 'ln' => '',
        'ci' => '',
        'st' => '',
        'sn' => '',
        'zip' => ''
    ];

    return $this->view->render(
        $response,
        "vytvorit-adresu.latte",
        $tplVars);

})->setName("vytvoritAdresu");

$app->post('/vytvorit-adresu', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();      //neco jako $_POST

    if(!empty($data['ci'])) {
        try {
            $this->db->beginTransaction();
            $idlocation = null;
            $st = empty($data['st']) ? null : $data['st'];
            $sn = empty($data['sn']) ? null : $data['sn'];
            $zip = empty($data['zip']) ? null : $data['zip'];

            $stmt = $this->db->prepare('INSERT INTO location(city,street_name,street_number,zip)
                                        VALUES(:ci,:st,:sn,:zip)');
            $stmt->bindValue(':ci', $data['ci']);
            $stmt->bindValue(':st', $st);
            $stmt->bindValue(':sn', $sn);
            $stmt->bindValue(':zip', $zip);
            $stmt->execute();
            $this->db->commit();
        } catch (Exception $ex) {
            $this->db->rollback();
            if ($ex->getCode() == 23505) {
                $tplVars['error'] = 'Tato adresa už existuje';
                $tplVars['form'] = $data;
                return $this->view->render(
                    $response,
                    "vytvorit-adresu.latte",
                    $tplVars);
            } else {
                $this->logger->error($ex->getMessage());
                die ($ex->getMessage());
            }
        }
        return $response->withHeader(
            'Location',
            $this->router->pathFor('osoby'));
    }
    //data neprisly
    $tplVars['form']=$data;
    $tplVars['error']='Město nesmí být prázdné';
    return $this->view->render(
        $response,
        "vytvorit-adresu.latte",
        $tplVars);

});

