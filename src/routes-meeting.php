<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/schuzky',
    function (Request $request, Response $response, $args) {
        try{
            $stmt = $this->db->prepare('SELECT meeting.*,a.pocet_ucastniku, 
                                    id_location, COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa
                                    FROM meeting 
                                    JOIN location USING (id_location)  
                                    LEFT JOIN(
                                         SELECT  id_meeting, COUNT(id_person) as pocet_ucastniku
                                         FROM person_meeting
                                         GROUP BY id_meeting
                                    ) AS a 
                                    ON a.id_meeting = meeting.id_meeting 
                                    ORDER BY meeting.start DESC, a.pocet_ucastniku DESC');
            $stmt->execute();
        }
        catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }

        $tplVars['meetings'] = $stmt->fetchAll();

        return $this->view->render(
            $response,
            "schuzky.latte",
            $tplVars);
    })->setName("schuzky");


$app->get('/pridej-schuzku', function (Request $request, Response $response, $args) {
    $tplVars['form']= [ 'ln' => '',
        'datum' => '',
        'descr' => '',
        'dur' => '',
        'ci' => '',
        'st' => '',
        'sn' => '',
        'zip' => ''
    ];
    $id = $request->getQueryParam('id');
    if(empty($id)) {
        exit('Chybí id osoby');
    }
    try{
        $stmt = $this->db->prepare('SELECT meeting.*,a.pocet_ucastniku, 
                                    id_location, COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa
                                    FROM meeting 
                                    JOIN person_meeting USING (id_meeting) 
                                    JOIN location USING (id_location)  
                                    LEFT JOIN(
                                         SELECT  id_meeting, COUNT(id_person) as pocet_ucastniku
                                         FROM person_meeting
                                         GROUP BY id_meeting
                                    ) AS a 
                                    ON a.id_meeting = meeting.id_meeting 
                                    WHERE person_meeting.id_person=:id
                                    ORDER BY meeting.start ASC, a.pocet_ucastniku DESC');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare('SELECT id_location, COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa 
                                     FROM location 
                                     ORDER BY 2 ASC');
        $stmt2->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $tplVars['meetings'] = $stmt->fetchAll();
    $tplVars['locations'] = $stmt2->fetchAll();
    $tplVars['id'] = $id;

    return $this->view->render(
        $response,
        "pridej-schuzku.latte",
        $tplVars);

})->setName("pridejSchuzku");

$app->post('/pridej-schuzku', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $descr = empty($data['descr']) ? '' :$data['descr'];
    $dur = empty($data['dur']) ? null :$data['dur'];

    if(!empty($data['id']) && !empty($data['datum'])) {
        try{
            $this->db->beginTransaction();
            /*
            $g = $data['g'];
            if (empty($data['g'])){
                $g=null;
            }*/

            $idlocation = null;
            if(($data['idl'])=="pna") {
                if(!empty($data['ci'])) {
                    //přidávám novou adresu
                    $ci = empty($data['ci']) ? null : $data['ci'];
                    $st = empty($data['st']) ? null : $data['st'];
                    $sn = empty($data['sn']) ? null : $data['sn'];
                    $zip = empty($data['zip']) ? null : $data['zip'];

                    $stmt = $this->db->prepare('INSERT INTO location(city,street_name,street_number,zip)
                                                VALUES(:ci,:st,:sn,:zip)');
                    $stmt->bindValue(':ci', $ci);
                    $stmt->bindValue(':st', $st);
                    $stmt->bindValue(':sn', $sn);
                    $stmt->bindValue(':zip', $zip);
                    $stmt->execute();
                    $idlocation = $this->db->lastInsertId('location_id_location_seq');
                }
            }
            else{
                //přidávám už existující adresu
                $idlocation = $data['idl'];
            }

            $stmt2 = $this->db->prepare('INSERT INTO meeting(start, description, duration, id_location)
                                    VALUES(:datum,:descr,:dur,:idl)');
            $stmt2->bindValue(':datum', $data['datum']);
            $stmt2->bindValue(':idl', $idlocation);
            $stmt2->bindValue(':dur', $dur);
            $stmt2->bindValue(':descr', $descr);
            $stmt2->execute();
            $idmeeting = $this->db->lastInsertId('meeting_id_meeting_seq');

            $stmt = $this->db->prepare('INSERT INTO person_meeting(id_person,id_meeting)
                                    VALUES(:idp,:idm)');
            $stmt->bindValue(':idp', $data['id']);
            $stmt->bindValue(':idm', $idmeeting);
            $stmt->execute();

            $this->db->commit();

        }
        catch (Exception $ex) {
            $this->db->rollback();
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }
        return $response->withHeader(
            'Location',
            $this->router->pathFor('pridejSchuzku').'?id='.$data['id']);

    }
    //data neprisly
    $tplVars['form']=$data;
    $tplVars['error']='Nejsou vyplněny všechny údaje';

    return $response->withHeader(
        'Location',
        $this->router->pathFor('pridejSchuzku').'?id='.$data['id']);
})->setName("pridejSchuzku");


$app->post('/odebrat-ze-schuzky', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $id_person = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_meeting=:id AND id_person = :idp');
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':idp', $id_person);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('pridejSchuzku').'?id='.$id_person);
})->setName('odebratZeSchuzky');


$app->post('/odebrat-schuzku', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    try{
        $stmt = $this->db->prepare('DELETE FROM meeting WHERE id_meeting=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('schuzky'));
})->setName('odebratSchuzku');

$app->post('/odebrat-osobu-ze-schuzky', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $id_person = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_meeting=:id AND id_person = :idp');
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':idp', $id_person);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('upravSchuzku').'?id='.$id);
})->setName('odebratOsobuZeSchuzky');


/*editace*/
$app->get('/uprav-schuzku', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $idperson = $request->getQueryParam('idperson');

    try{
        $stmt = $this->db->prepare('SELECT *, CAST(start as date) || \'T\' || LEFT((CAST(start as time)::VARCHAR),5) as zacatek FROM meeting WHERE id_meeting=:id');
        $stmt->bindValue(':id',$id);
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare('SELECT *
                                    FROM person_meeting
                                    JOIN person USING(id_person) 
                                    WHERE id_meeting=:id
                                    ');
        $stmt2->bindValue(':id',$id);
        $stmt2->execute();
    }
    catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt3 = $this->db->prepare('SELECT * 
                                     FROM person 
                                     WHERE id_person NOT IN (
                                                              SELECT a.id_person
                                                              FROM person_meeting a
                                                              WHERE a.id_meeting=:id
                                                              ) 
                                     ORDER BY last_name');
        $stmt3->bindValue(':id', $id);
        $stmt3->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt4 = $this->db->prepare('SELECT id_location,COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa 
                                     FROM location');
        $stmt4->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $meetings = $stmt->fetch();
    $tplVars['form']= [
        'descr' => $meetings['description'],
        'datum' => $meetings['zacatek'],
        'dur' => $meetings['duration'],
        'idl' => $meetings['id_location']
    ];
    $tplVars['participants'] = $stmt2->fetchAll();
    $tplVars['persons'] = $stmt3->fetchAll();
    $tplVars['locations'] = $stmt4->fetchAll();
    $tplVars['id']=$id;
    $tplVars['idperson']=$idperson;
    return $this->view->render($response,'uprav-schuzku.latte',$tplVars);
})->setName('upravSchuzku');

/*ulozeni zmen*/
$app->post('/uprav-schuzku', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $data = $request->getParsedBody();
    $descr = empty($data['descr']) ? '' :$data['descr'];
    $dur = empty($data['dur']) ? null :$data['dur'];

    try{
        $stmt = $this->db->prepare('UPDATE meeting 
                                    SET   description=:de,
                                          id_location=:idl,
                                          start=:st,
                                          duration=:dur
                                    WHERE id_meeting=:id');
        $stmt->bindValue(':de', $descr);
        $stmt->bindValue(':idl', $data['idl']);
        $stmt->bindValue(':dur', $dur);
        $stmt->bindValue(':st', $data['datum']);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    return $response->withHeader('Location',$this->router->pathFor('upravSchuzku').'?id='.$id);
});


$app->get('/vytvorit-schuzku', function (Request $request, Response $response, $args) {
    $tplVars['form']= [ 'ln' => '',
        'datum' => '',
        'descr' => '',
        'dur' => '',
        'ci' => '',
        'st' => '',
        'sn' => '',
        'zip' => ''
    ];
    try{
        $stmt = $this->db->prepare('SELECT id_location, COALESCE(city,\'\') || \' \' || COALESCE(street_name,\'\') || \' \' || COALESCE(street_number::VARCHAR,\'\') ||  COALESCE(\' (\' ||location.name|| \')\',\'\')  as adresa 
                                     FROM location 
                                     ORDER BY 2 ASC');
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    $tplVars['locations'] = $stmt->fetchAll();

    return $this->view->render(
        $response,
        "vytvorit-schuzku.latte",
        $tplVars);

})->setName("vytvoritSchuzku");

$app->post('/vytvorit-schuzku', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    $descr = empty($data['descr']) ? '' :$data['descr'];
    $dur = empty($data['dur']) ? null :$data['dur'];

    if(!empty($data['datum']) and ($data['idl']!="pna" or (($data['idl']=="pna") and !empty($data['ci'])))) {
        try{
            $this->db->beginTransaction();
            /*
            $g = $data['g'];
            if (empty($data['g'])){
                $g=null;
            }*/

            $idlocation = null;
            if(($data['idl'])=="pna") {
                if(!empty($data['ci'])) {
                    //přidávám novou adresu
                    $ci = empty($data['ci']) ? null : $data['ci'];
                    $st = empty($data['st']) ? null : $data['st'];
                    $sn = empty($data['sn']) ? null : $data['sn'];
                    $zip = empty($data['zip']) ? null : $data['zip'];

                    $stmt = $this->db->prepare('INSERT INTO location(city,street_name,street_number,zip)
                                                VALUES(:ci,:st,:sn,:zip)');
                    $stmt->bindValue(':ci', $ci);
                    $stmt->bindValue(':st', $st);
                    $stmt->bindValue(':sn', $sn);
                    $stmt->bindValue(':zip', $zip);
                    $stmt->execute();
                    $idlocation = $this->db->lastInsertId('location_id_location_seq');
                }
                else{
                    throw new Exception('Empty address');
                }
            }
            else{
                //přidávám už existující adresu
                $idlocation = $data['idl'];
            }

            $stmt2 = $this->db->prepare('INSERT INTO meeting(start, description, duration, id_location)
                                    VALUES(:datum,:descr,:dur,:idl)');
            $stmt2->bindValue(':datum', $data['datum']);
            $stmt2->bindValue(':idl', $idlocation);
            $stmt2->bindValue(':dur', $dur);
            $stmt2->bindValue(':descr', $descr);
            $stmt2->execute();
            $idmeeting = $this->db->lastInsertId('meeting_id_meeting_seq');

            $this->db->commit();
        }
        catch (Exception $ex) {
            $this->db->rollback();
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }

        $tplVars['error']=$data['datum'];

        return $response->withHeader(
            'Location',
            $this->router->pathFor('upravSchuzku').'?id='.$idmeeting);

    }
    //data neprisly
    $tplVars['form']=$data;
    $tplVars['error']='Nejsou vyplněny všechny údaje';

    return $this->view->render(
        $response,
        "vytvorit-schuzku.latte",
        $tplVars);
})->setName("vytvoritSchuzku");

$app->post('/pridej-osobu-ke-schuzce', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if(!empty($data['id']) && !empty($data['pr_id'])) {
        try{
            $stmt = $this->db->prepare('INSERT INTO person_meeting(id_meeting,id_person)
                                        VALUES(:id_m,:id_pr)');
            $stmt->bindValue(':id_m', $data['id']);
            $stmt->bindValue(':id_pr', $data['pr_id']);
            $stmt->execute();
        }
        catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }
    }else{
        exit('Specify required parameters.');
    }
    return $response->withHeader(
        'Location',
        $this->router->pathFor('upravSchuzku').'?id='.$data['id']);
})->setName("pridejOsobuKeSchuzce");


