<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/pridej-kontakt', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    if(empty($id)) {
        exit('Chybí id osoby');
    }
    try{
        $stmt = $this->db->prepare('SELECT * FROM contact_type');
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare(' SELECT id_contact,contact, name, id_person 
                                      FROM contact 
                                      JOIN contact_type USING (id_contact_type) 
                                      WHERE id_person=:id');
        $stmt2->bindValue(':id', $id);
        $stmt2->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $tplVars['contacts'] = $stmt2->fetchAll();
    $tplVars['types'] = $stmt->fetchAll();
    $tplVars['id'] = $id;

    return $this->view->render(
        $response,
        "pridej-kontakt.latte",
        $tplVars);

})->setName("pridejKontakt");

$app->post('/pridej-kontakt', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if(!empty($data['id']) && !empty($data['ct_id']) && !empty($data['txt'])) {
        try{
            $stmt = $this->db->prepare('INSERT INTO contact(id_person,id_contact_type,contact)
                                        VALUES(:id_p,:id_ct,:con)');
            $stmt->bindValue(':id_p', $data['id']);
            $stmt->bindValue(':id_ct', $data['ct_id']);
            $stmt->bindValue(':con', $data['txt']);

            $stmt->execute();
        }
        catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }
    }else{
        exit('Specify required parameters.');
    }
    return $response->withHeader(
        'Location',
        $this->router->pathFor('pridejKontakt').'?id='.$data['id']);
})->setName("pridejKontakt");


$app->post('/smaz-kontakt', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $id_person = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_contact=:id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex)
    {$this->logger->error($ex->getMessage());
        die ($ex->getMessage());}

    return $response->withHeader('Location',$this->router->pathFor('pridejKontakt').'?id='.$id_person);
})->setName('smazKontakt');


/*editace*/
$app->get('/uprav-kontakt', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $idperson = $request->getQueryParam('idperson');
    try{
        $stmt = $this->db->prepare('SELECT * FROM contact JOIN contact_type USING(id_contact_type) WHERE id_contact=:id');
        $stmt->bindValue(':id',$id);
        $stmt->execute();
    }
    catch (Exception $ex) {
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }
    try{
        $stmt2 = $this->db->prepare('SELECT * FROM contact_type');
        $stmt2->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    $contacts = $stmt->fetch();
    $tplVars['form']= [
        'contact' => $contacts['contact'],
        'type' => $contacts['id_contact_type']
    ];
    $tplVars['types'] = $stmt2->fetchAll();
    $tplVars['id']=$id;
    $tplVars['idperson']=$idperson;
    return $this->view->render($response,'uprav-kontakt.latte',$tplVars);
})->setName('upravKontakt');

/*ulozeni zmen*/
$app->post('/uprav-kontakt', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id');
    $idperson = $request->getQueryParam('idperson');
    $data = $request->getParsedBody();
    try{
        $stmt = $this->db->prepare('UPDATE contact 
                                    SET   contact = :contact,
                                          id_contact_type = :ict
                                    WHERE id_contact=:id');
        $stmt->bindValue(':contact', $data['contact']);
        $stmt->bindValue(':ict', $data['ict']);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
    catch (Exception $ex){
        $this->logger->error($ex->getMessage());
        die ($ex->getMessage());
    }

    return $response->withHeader('Location',$this->router->pathFor('pridejKontakt').'?id='.$idperson);
    //return $response->withHeader('Location',$this->router->pathFor('osoby'));
});

