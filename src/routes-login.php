<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/login', function (Request $request, Response $response, $args) {
    return $this->view->render(
        $response,
        "login.latte");
})->setName("login");


$app->post('/login', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (!empty($data['login']) && !empty($data['pass'])) {
        try {
            $stmt = $this->db->prepare('SELECT * FROM account WHERE login = :l');
            $stmt->bindValue(':l', $data['login']);
            $stmt->execute();
            $user = $stmt->fetch();
            if (!empty($user)) {
                if (password_verify($data['pass'], $user['password'])) {
                    $_SESSION['user'] = $user;
//password_hash();  //takhle si vytvorim z plaintext hesla jeho hash
                    return $response->withHeader(
                        'Location',
                        $this->router->pathFor('osoby'));
                }
            }
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            die ($ex->getMessage());
        }

    }
    $tplVars['error'] = 'Chyba přihlášení';
    return $this->view->render(
        $response,
        "login.latte",
        $tplVars);

});

$app->get('/registrace', function (Request $request, Response $response, $args) {
    return $this->view->render(
        $response,
        "registrace.latte");
})->setName("registrace");

$app->post('/registrace', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (!empty($data['login']) && !empty($data['pass1']) && !empty($data['pass2'])) {
        if($data['pass1']==$data['pass2'])
        {
            $hashHesla = password_hash($data['pass1'],PASSWORD_DEFAULT);
            try {
                $stmt = $this->db->prepare('INSERT INTO account(login,password) VALUES(:l,:h)');
                $stmt->bindValue(':l', $data['login']);
                $stmt->bindValue(':h', $hashHesla);

                $stmt->execute();
                $stmt->fetch();

                return $response->withHeader(
                    'Location',
                    $this->router->pathFor('login'));


            } catch (Exception $ex) {
                if($ex->getCode() == 23505) {
                    $tplVars['error']='Tento login už existuje.';
                    $tplVars['form']=$data;
                    return $this->view->render(
                        $response,
                        "registrace.latte",
                        $tplVars);
                }else {
                    $this->logger->error($ex->getMessage());
                    die ($ex->getMessage());
                }
            }
        }
        else
        {
            $tplVars['error'] = 'Zadaná hesla se neshodují.';
        }
    }

    if (empty($tplVars['error'])){
        $tplVars['error'] = 'Chyba registrace.';
    }

    return $this->view->render(
        $response,
        "registrace.latte",
        $tplVars);

});
